(function ($) {
    Drupal.behaviors.clickToCallForm = {
        attach: function (context, settings) {
            jQuery('.akuma-tokenfield').each(function (index, element) {
                    jQuery(element)
                        .on('tokenfield:createtoken', function (e) {
//                            console.log('tokenfield:createtoken');
                        })

                        .on('tokenfield:createdtoken', function (e) {
//                            console.log('tokenfield:createdtoken');
                        })

                        .on('tokenfield:edittoken', function (e) {
//                            console.log('tokenfield:edittoken');
                        })

                        .on('tokenfield:removedtoken', function (e) {
//                            console.log('tokenfield:removedtoken');
                        });
                    var auto_input = jQuery('#' + jQuery(element).prop('id') + '-autocomplete');
                    if (auto_input && auto_input.length) {
                        jQuery(element).tokenfield({
                                autocomplete           : {
                                    source   : function (request, response) {
                                        $.ajax({
                                            type       : "POST",
                                            contentType: "application/json; charset=utf-8",
                                            url        : auto_input.val() + '/' + request.term,
                                            dataType   : "json",
                                            success    : function (data) {
                                                /**
                                                 * Clear given data from html tags
                                                 */
                                                console.log(data);
                                                for (var i in data) {
                                                    data[i] = {
                                                        'label' : jQuery(data[i]).text().length?jQuery(data[i]).text():data[i],
                                                        'value' : i
                                                    };
                                                }
                                                console.log(data);
                                                response(data);
                                            }
                                        });
                                    },
                                    minLength: 3,
                                    delay    : 100
                                },
                                showAutocompleteOnFocus: true
                            }
                        )
                    }
//                    jQuery(element).tokenfield('setTokens', [
//                        { value: "blue", label: "Blau" },
//                        { value: "red", label: "Rot" }
//                    ]);
                }
            );
        }
    };
})(jQuery);
