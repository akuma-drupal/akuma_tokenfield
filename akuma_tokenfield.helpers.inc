<?php
/**
 * User  : Nikita.Makarov
 * E-Mail: mesaverde228@gmail.com
 */
if (!function_exists('pre')) {
  /**
   * Similar to "var_dump"
   *
   * @param $_
   */
  function pre($_) {
    $args = func_get_args();
    if (count($args)) {
      foreach ($args as $arg) {
        echo "<pre>" . htmlentities(
            (is_array($arg) || is_object($arg)) ? print_r($arg, TRUE) : $arg
          ) . '</pre>';
        @flush();
        @ob_flush();
      }

    }
  }
}