<?php
/**
 * User  : Nikita Makarov
 * Date  : 11/24/15
 * E-Mail: mesaverde228@gmail.com
 * 
 * @file 
 * Description
 */

$plugin = array(
  'title' => t('Bootstrap Behavior Handler'),
  'class' => 'BootstrapBehaviorHandler',
);