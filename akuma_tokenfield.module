<?php
defined("DS") || define("DS", DIRECTORY_SEPARATOR);
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "akuma_tokenfield.helpers.inc";
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "akuma_tokenfield.theme.inc";

function akuma_tokenfield_menu()
{
    $file_path = drupal_get_path('module', 'akuma_tokenfield');
    $items = array();

    $items['admin/config/akuma'] = array(
      'title'            => 'Akuma',
      'description'      => '',
      'position'         => 'left',
      'weight'           => -10,
      'page callback'    => 'system_admin_menu_block_page',
      'access arguments' => array('administer site configuration'),
      'file path'        => drupal_get_path('module', 'system'),
      'file'             => 'system.admin.inc',
    );


    $items['admin/config/akuma/tokenfield'] = array(
      'title'            => 'Bootstrap Tokenfield',
      'description'      => '',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('akuma_tokenfield_admin_settings_form'),
      'access arguments' => array('administer site configuration'),
      'file path'        => $file_path,
      'file'             => 'akuma_tokenfield.admin.inc',
      'weight'           => 1,
    );


    $items['akuma/autocomplete/%/%/%'] = array(
      'title'            => 'Entity Reference Bootstrap Autocomplete',
      'page callback'    => 'akuma_tokenfield_autocomplete_callback', //'entityreference_autocomplete_callback',
      'page arguments'   => array(1, 2, 3, 4),
      'access callback'  => 'entityreference_autocomplete_access_callback',
      'access arguments' => array(1, 2, 3, 4),
      'type'             => MENU_CALLBACK,
    );


    return $items;
}

/**
 * @param        $type
 * @param        $field_name
 * @param        $entity_type
 * @param        $bundle_name
 * @param string $entity_id
 * @param string $string
 *
 * @return int
 */
function akuma_tokenfield_autocomplete_callback(
  $type,
  $field_name,
  $entity_type,
  $bundle_name,
  $entity_id = '',
  $string = ''
) {
    // If the request has a '/' in the search text, then the menu system will have
    // split it into multiple arguments and $string will only be a partial. We want
    //  to make sure we recover the intended $string.
    $args = func_get_args();
    // Shift off the $type, $field_name, $entity_type, $bundle_name, and $entity_id args.
    array_shift($args);
    array_shift($args);
    array_shift($args);
    array_shift($args);
    array_shift($args);
    $string = implode('/', $args);

    $field = field_info_field($field_name);
    $instance = field_info_instance($entity_type, $field_name, $bundle_name);

    return akuma_tokenfield_autocomplete_callback_get_matches(
      $type,
      $field,
      $instance,
      $entity_type,
      $entity_id,
      $string
    );
}

/**
 * Implements hook_field_widget_info().
 */
function akuma_tokenfield_field_widget_info()
{
    $widgets = array();

    $widgets['entityreference_autocomplete_tokenfield'] = array(
      'label'       => t('Bootstrap TokenField'),
      'weight'      => -999, // At the top of options && default
      'description' => t('An autocomplete text field in bootstrap styling.'),
      'field types' => array(
        'entityreference'
      ),
      'settings'    => array(
        'match_operator' => 'CONTAINS',
        'size'           => 60,
        'path'           => '', //'akuma/autocomplete',
      ),
      'behaviors'   => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
    );

    return $widgets;
}

function akuma_tokenfield_field_widget_info_alter(&$widgets)
{
    if (isset($widgets['taxonomy_autocomplete'])) {
        $widgets['taxonomy_autocomplete_tokenfield'] = $widgets['taxonomy_autocomplete'];
        $widgets['taxonomy_autocomplete_tokenfield']['label'] = t('Bootstrap TokenField');
    }
}


function akuma_tokenfield_field_widget_taxonomy_autocomplete_tokenfield_form_alter(&$element, &$form_state, $context)
{
    $element += array(
      '#attributes' => array(
        'class' => array('akuma-tokenfield', 'form-control')
      )
    );
    $module_path = drupal_get_path('module', 'akuma_tokenfield');
    $element['#attached']['css'][] = $module_path . '/css/bootstrap-tokenfield.css';
    $element['#attached']['css'][] = $module_path . '/css/tokenfield-typeahead.css';

    $element['#attached']['js'][] = $module_path . '/js/bootstrap-tokenfield.js';
    $element['#attached']['js'][] = $module_path . '/js/tokenfield-behavior.js';
    $element['#attached']['library'][] = array('system', 'ui.autocomplete');
}


/**
 * Implements hook_field_widget_settings_form().
 */
function akuma_tokenfield_field_widget_settings_form($field, $instance)
{
    $widget = $instance['widget'];
    $settings = $widget['settings'] + field_info_widget_settings($widget['type']);

    $form = array();

    if ($widget['type'] == 'entityreference_autocomplete_tokenfield') {
        $form['match_operator'] = array(
          '#type'          => 'select',
          '#title'         => t('Autocomplete matching'),
          '#default_value' => $settings['match_operator'],
          '#options'       => array(
            'STARTS_WITH' => t('Starts with'),
            'CONTAINS'    => t('Contains'),
          ),
          '#description'   => t(
            'Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of nodes.'
          ),
        );
        $form['size'] = array(
          '#type'             => 'textfield',
          '#title'            => t('Size of textfield'),
          '#default_value'    => $settings['size'],
          '#element_validate' => array('_element_validate_integer_positive'),
          '#required'         => true,
        );
    }

    return $form;
}


function _akuma_tokenfield_autocomplete_validate($element, &$form_state, $form)
{
    _entityreference_autocomplete_tags_validate($element, $form_state, $form);
}

/**
 * Return JSON based on given field, instance and string.
 *
 * This function can be used by other modules that wish to pass a mocked
 * definition of the field on instance.
 *
 * @param $type
 *   The widget type (i.e. 'single' or 'tags').
 * @param $field
 *   The field array defintion.
 * @param $instance
 *   The instance array defintion.
 * @param $entity_type
 *   The entity type.
 * @param $entity_id
 *   Optional; The entity ID the entity-reference field is attached to.
 *   Defaults to ''.
 * @param $string
 *   The label of the entity to query by.
 */
function akuma_tokenfield_autocomplete_callback_get_matches(
  $type,
  $field,
  $instance,
  $entity_type,
  $entity_id = '',
  $string = ''
) {
    $matches = array();

    $entity = null;
    if ($entity_id !== 'NULL') {
        $entity = entity_load_single($entity_type, $entity_id);
        $has_view_access = (entity_access('view', $entity_type, $entity) !== false);
        $has_update_access = (entity_access('update', $entity_type, $entity) !== false);
        if (!$entity || !($has_view_access || $has_update_access)) {
            return MENU_ACCESS_DENIED;
        }
    }

    $handler = entityreference_get_selection_handler($field, $instance, $entity_type, $entity);

    if ($type == 'akuma') {
        // The user enters a comma-separated list of tags. We only autocomplete the last tag.
        $tags_typed = drupal_explode_tags($string);
        $tag_last = drupal_strtolower(array_pop($tags_typed));
        if (!empty($tag_last)) {
            $prefix = count($tags_typed) ? implode(', ', $tags_typed) . ', ' : '';
        }
    } else {
        // The user enters a single tag.
        $prefix = '';
        $tag_last = $string;
    }

    if (isset($tag_last)) {
        // Get an array of matching entities.
        $entity_labels = $handler->getReferencableEntities(
          $tag_last,
          $instance['widget']['settings']['match_operator'],
          10
        );

        // Loop through the products and convert them into autocomplete output.
        foreach ($entity_labels as $values) {
            foreach ($values as $entity_id => $label) {
                $key = "$label ($entity_id)";
                // Strip things like starting/trailing white spaces, line breaks and tags.
                $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(decode_entities(strip_tags($key)))));
                // Names containing commas or quotes must be wrapped in quotes.
                if (strpos($key, ',') !== false || strpos($key, '"') !== false) {
                    $key = '"' . str_replace('"', '""', $key) . '"';
                }
                $matches[$prefix . $key] = '<div class="reference-autocomplete">' . $label . '</div>';
            }
        }
    }

    drupal_json_output($matches);
}

/**
 * Implements hook_field_widget_form().
 */
function akuma_tokenfield_field_widget_form(
  &$form,
  &$form_state,
  $field,
  $instance,
  $langcode,
  $items,
  $delta,
  $element
) {
    // Ensure that the entity target type exists before displaying the widget.
    $entity_info = entity_get_info($field['settings']['target_type']);
    if (empty($entity_info)) {
        return;
    }
    $entity_type = $instance['entity_type'];
    $entity = isset($element['#entity']) ? $element['#entity'] : null;
    $handler = entityreference_get_selection_handler($field, $instance, $entity_type, $entity);

    if ($instance['widget']['type'] == 'entityreference_autocomplete_tokenfield') {

        if ($instance['widget']['type'] == 'entityreference_autocomplete') {
            // We let the Field API handles multiple values for us, only take
            // care of the one matching our delta.
            if (isset($items[$delta])) {
                $items = array($items[$delta]);
            } else {
                $items = array();
            }
        }

        $entity_ids = array();
        $entity_labels = array();

        // Build an array of entities ID.
        foreach ($items as $item) {
            $entity_ids[] = $item['target_id'];
        }

        // Load those entities and loop through them to extract their labels.
        $entities = entity_load($field['settings']['target_type'], $entity_ids);

        foreach ($entities as $entity_id => $entity_item) {
            $label = $handler->getLabel($entity_item);
            $key = "$label ($entity_id)";
            // Labels containing commas or quotes must be wrapped in quotes.
            if (strpos($key, ',') !== false || strpos($key, '"') !== false) {
                $key = '"' . str_replace('"', '""', $key) . '"';
            }
            $entity_labels[] = $key;
        }

        // Prepare the autocomplete path.
        if (!empty($instance['widget']['settings']['path'])) {
            $autocomplete_path = $instance['widget']['settings']['path'];
        } else {
            $autocomplete_path = $instance['widget']['type'] == 'entityreference_autocomplete' ? 'entityreference/autocomplete/single' : 'entityreference/autocomplete/tags';
        }

        $autocomplete_path .= '/' . $field['field_name'] . '/' . $instance['entity_type'] . '/' . $instance['bundle'] . '/';

        // Use <NULL> as a placeholder in the URL when we don't have an entity.
        // Most webservers collapse two consecutive slashes.
        $id = 'NULL';
        if ($entity) {
            list($eid) = entity_extract_ids($entity_type, $entity);
            if ($eid) {
                $id = $eid;
            }
        }
        $autocomplete_path .= $id;

        $module_path = drupal_get_path('module', 'akuma_tokenfield');
        $form['#attached']['css'][] = $module_path . '/css/bootstrap-tokenfield.css';
        $form['#attached']['css'][] = $module_path . '/css/tokenfield-typeahead.css';

        $form['#attached']['js'][] = $module_path . '/js/bootstrap-tokenfield.js';
        $form['#attached']['js'][] = $module_path . '/js/tokenfield-behavior.js';
        $form['#attached']['library'][] = array('system', 'ui.autocomplete');

        $element += array(
          '#type'              => 'textfield',
          '#maxlength'         => 1024,
          '#default_value'     => implode(', ', $entity_labels),
          '#autocomplete_path' => $autocomplete_path,
          '#size'              => $instance['widget']['settings']['size'],
          '#element_validate'  => array('_akuma_tokenfield_autocomplete_validate'),
          '#attributes'        => array(
            'class' => array('akuma-tokenfield', 'form-control')
          )
        );

        return $element;
    }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function akuma_tokenfield_ctools_plugin_directory($module, $plugin)
{
//    if ($module == 'entityreference') {
//        return 'plugins/entityreference/' . $plugin;
//    }
}