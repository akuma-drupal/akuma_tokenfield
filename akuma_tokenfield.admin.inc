<?php
/**
 * User  : Nikita.Makarov
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Admin page callbacks for the Akuma Core module.
 */

/**
 * Form builder; Configure settings.
 *
 * @ingroup forms
 *
 * @see     system_settings_form()
 */
function akuma_tokenfield_admin_settings_form($form, $form_state)
{
    drupal_set_title(t('Akuma: Tokenizer'));

    $form = array();
    // Clear the cache bins on submit.
    $form['#submit'][] = 'akuma_tokenfield_admin_settings_form_submit';

    return system_settings_form($form);
}

// Submit callback.
/**
 * STUB
 */
function akuma_tokenfield_admin_settings_form_submit($form, &$form_state)
{

}